import sys
import json
import re
class mysql_bq_tableSchema(object):

    def __init__(self, raw_schema):
        self.raw = raw_schema
        self.listed_cols = self.preprocess_cols()
        self.mysql_cols = []
        self.big_query_schema = []

    def preprocess_cols(self):
        cols = [element for element in self.raw if re.match("^ *`.*` [a-z]*(.*) [A-Z]* [A-Z]*$", element)]
        #cols = [element for element in self.raw if re.match("^ *`.*` [a-z]*\(.*$", element)]
        parsed_cols = [ element.split() for element in cols]
        for each_col in parsed_cols:
            each_col[0] = re.sub("`","",each_col[0])
            each_col[1] = re.sub("\(.*\)","",each_col[1])
            #print each_col
        return parsed_cols

    data_types = {
#        'string'
        'char':'string',

        'character':'string',
        'varchar':'string',
        'tinytext':'string',
        'text':'string',
        'mediumtext':'string',
        'longtext':'string',
#        'integer'
        'tinyint':'integer',
        'smallint':'integer',
        'mediumint':'integer',
        'integer':'integer',
        'int':'integer',
        'bigint':'integer',
#        'float'
        'float':'float',
        'double':'float',
        'real':'float',
        'decimal':'float',
        'fixed':'float',
        'dec':'float',
        'numeric':'float',
#        'timestamp'
        'date':'timestamp',
        'datetime':'timestamp',
        'timestamp':'timestamp',
        'time':'timestamp',
#        'boolean'
        'bit':'boolean',
        'bool':'boolean',
        'boolean':'boolean',
    }

    def convert_type(self, mysql_type):
        return self.data_types.get(mysql_type, None)

    def parse(self):
        for item in self.listed_cols:
            self.mysql_cols.append([item[0],self.convert_type(item[1]),item[2],item[len(item)-1]])
            #element 1: type, 2: default, 3: default value
        return self.mysql_cols

    def is_nullable(self, is_required, default_value):
        return 'nullable' if (is_required == 'DEFAULT' and default_value == 'NULL') else 'required'

    def mysql_to_big_query_schema(self):
        for item in self.mysql_cols:
            self.big_query_schema.append(json.dumps({
                'name': item[0],
                'type': item[1],
                'mode': self.is_nullable(item[2],item[3])
            }, separators=(',',':')))
        return ','.join(self.big_query_schema)

if __name__ == '__main__':
    mysql_create_table_query = sys.stdin
    big_query = mysql_bq_tableSchema(mysql_create_table_query.readlines())
    big_query.parse()
    big_query_json = big_query.mysql_to_big_query_schema()
    print big_query_json
